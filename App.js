import { NavigationContainer } from '@react-navigation/native';
import Routes from './routes';
import Toast from 'react-native-toast-message'
import toastConfig from './src/components/Toast/CustomToast';
import { Provider } from 'react-redux';
import { persistor, store } from './src/redux/store';
import { PersistGate } from 'redux-persist/integration/react'
import { Text } from 'react-native';

export default function App() {
  return (
    <Provider store={store}>
      <PersistGate loading={<Text>Loading...</Text>} persistor={persistor}>
        <NavigationContainer>
          <Routes />
          <Toast config={toastConfig} />
        </NavigationContainer>
      </PersistGate>
    </Provider>
  );
}