# Install instructions
## About
A Food ordering app built using React Native and Supabase. This app is built with the help of [Expo](https://expo.dev).
## Installation
App requires [Node.js](https://nodejs.org/), NPM and [Expo Go](https://expo.dev/client) mobile app to run.

Install the dependencies and devDependencies and start the server.

```sh
cd coffee-shop
npx expo install
npm run start
```
# Screenshots

![Alt text](https://gitlab.com/adityasovani/food-ordering-app/-/raw/master/assets/Screenshots/home.jpg?ref_type=heads) &nbsp;![Alt text](https://gitlab.com/adityasovani/food-ordering-app/-/raw/master/assets/Screenshots/PDP.jpg?ref_type=heads) &nbsp; ![Alt text](https://gitlab.com/adityasovani/food-ordering-app/-/raw/master/assets/Screenshots/cart.jpg?ref_type=heads) 
