module.exports = {
    project: {
        ios: {},
        android: {}, // grouped into "project"
    },
    assets: ["./assets/fonts/"], // stays the same
    supabase: {
        client: {
            auth: {
                persistSession: false //or true
            }
        }
    }
};