import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React from 'react'
import Launch from './src/screens/Launch/Launch';
import Login from './src/screens/Login/Login';
import Dashboard from './src/screens/Dashboard/Dashboard';
import ProductDetails from './src/screens/ProductDetails/ProductDetails';
import Cart from './src/screens/Cart/Cart';
import AddressSelection from './src/screens/AddressSelection/AddressSelection';
import PaymentMethod from './src/screens/PaymentMethod/PaymentMethod';

const Stack = createNativeStackNavigator();


const Routes = () => {

    return (
        <Stack.Navigator screenOptions={{ headerShown: false }}>
            <Stack.Screen name='Launch' component={Launch} />
            <Stack.Screen name='Login' component={Login} />
            <Stack.Screen name='Dashboard' component={Dashboard} />
            <Stack.Screen name='ProductDetails' component={ProductDetails} initialParams={{ id: 0 }} />
            <Stack.Screen name='Cart' component={Cart} />
            <Stack.Screen name='AddressSelection' component={AddressSelection} />
            <Stack.Screen name='PaymentMethod' component={PaymentMethod} />
        </Stack.Navigator>
    )
}

export default Routes;