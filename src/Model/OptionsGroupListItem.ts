type OptionsGroupListItem = {
    name: string;
    icon?: any;
    color?: string;
}