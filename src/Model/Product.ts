type Product = {
    id: number,
    name: string,
    thumbnail_url: string,
    price: number,
    description: string
};

export default Product;