type Profile = {
    id: number,
    name: string,
    address: string,
    email: string,
    profile_picture_url: string
};