import { Icon } from "@rneui/themed";
import React, { useState } from "react";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { Card } from "react-native-paper";
import GestureRecognizer from "react-native-swipe-gestures";
import { useDispatch } from "react-redux";
import { addToFavorites, removeFromCart } from "../../redux/action";
import Toast from "react-native-toast-message";

type Props = {
    product: any
};

const CartProductCard: React.FC<Props> = ({ product }) => {
    const [left, setleft] = useState(0);
    const dispatch = useDispatch();
    const [favoriteIcon, setfavoriteIcon] = useState('favorite-outline');

    const handleRemoveFromCart = () => {
        let toastContent = {
            type: 'success', text1: 'Done',
            text2: 'Product removed from cart!', time: 2000
        };
        try {
            dispatch(removeFromCart(product.id));
        } catch (err) {
            toastContent = {
                type: 'error', text1: 'Error',
                text2: 'Error adding product to your cart. Please try again', time: 3000
            }
        } finally {
            setleft(0);
            Toast.show(toastContent);
        }
    };

    const handleAddToFavorites = () => {
        let toastContent = {
            type: 'success', text1: 'Done',
            text2: 'Added product to favourites!', time: 2000
        };
        try {
            dispatch(addToFavorites(product));
            setfavoriteIcon("favorite");
        } catch (err) {
            toastContent = {
                type: 'error', text1: 'Error',
                text2: 'Error adding product to favorites. Please try again', time: 3000
            }
        } finally {
            setleft(0);
            Toast.show(toastContent);
        }
    };

    return <GestureRecognizer
        onSwipeLeft={() => setleft(-50)}
        onSwipeRight={() => setleft(0)}
        style={[left ? { flexDirection: 'row', alignItems: 'center' } : {}, { marginBottom: 15 }]}
    >
        <Card style={[styles.productCard, { left: left }]}
            contentStyle={{ flexDirection: 'row', borderRadius: 20 }}>
            <Image source={{ uri: product.thumbnail_url }}
                style={styles.thumbnail} />
            <View>
                <Text style={styles.productName}>{product.name}</Text>
                <Text style={styles.productPrice}>₹ {product.price}</Text>
            </View>
            <View style={styles.quantityStepperContainer}>
                <Text style={[styles.stepperText, { marginLeft: 7, color: 'white' }, { letterSpacing: -1 }]}>--</Text>
                <Text style={styles.stepperText}>1</Text>
                <Text style={[styles.stepperText]}>+</Text>
            </View>
        </Card>
        {left ? <>
            <View style={{ flexDirection: 'row' }}>
                <TouchableOpacity
                    style={[styles.slideActionIcon, { marginRight: 5 }]}
                    onPress={handleAddToFavorites}
                >
                    <Icon name={favoriteIcon} color='#FFF' />
                </TouchableOpacity>
                <TouchableOpacity style={[styles.slideActionIcon]}
                    onPress={handleRemoveFromCart}
                >
                    <Icon name="delete-outline" color='#FFF' />
                </TouchableOpacity>
            </View>
        </> : null}
    </GestureRecognizer>;
};

const styles = StyleSheet.create({
    productCard: {
        width: '80%',
        alignSelf: 'center',
        borderRadius: 20,
        height: 100
    },
    thumbnail: {
        height: 80,
        width: 80,
        alignSelf: 'center',
        marginTop: 10,
        marginLeft: 10,
        marginRight: 10,
    },
    productName: {
        color: '#000',
        fontSize: 16,
        fontWeight: '600',
        marginTop: 20
    },
    productPrice: {
        marginTop: 10,
        color: '#FA4A0C',
        fontWeight: '600',
        fontSize: 15
    },
    quantityStepperContainer: {
        width: 70,
        height: 25,
        flexShrink: 0,
        borderRadius: 30,
        backgroundColor: '#FA4A0C',
        alignSelf: 'flex-end',
        flexDirection: 'row',
        justifyContent: 'center',
        position: 'absolute',
        right: 20
    },
    stepperText: {
        color: '#FFF',
        marginRight: 10
    },
    slideActionIcon: {
        height: 50, width: 50,
        borderRadius: 90,
        backgroundColor: '#DF2C2C',
        alignItems: 'center',
        justifyContent: 'center',
        left: -30,
    }
});

export default CartProductCard;