import React from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'
import { Appbar } from 'react-native-paper';
import Searchbar from '../SearchBar/Searchbar';
import FoodCategoriesTab from './FoodCategoriesTab';
import { useNavigation } from '@react-navigation/native';

type Props = {
    title: string;
};

const DashboardHeader: React.FC<Props> = ({ title }) => {

    const navigation = useNavigation<any>();

    return (
        <>
            <View style={{ backgroundColor: '#F2F2F2', height: '48%', marginLeft: 20 }}>
                <Appbar.Header style={{ marginTop: 30, backgroundColor: '#F2F2F2', display: 'flex', width: '100%' }}>
                    <Appbar.Action icon={require('../../../assets/images/HamburgerMenu.png')} />
                    <Appbar.Action icon='cart' style={{ right: '-350%' }} onPress={() => navigation.navigate('Cart')} />
                </Appbar.Header>
                <View style={styles.headerTextContainer}>
                    <Text style={styles.headerText}>{title}</Text>
                </View>
                <View style={styles.searchContainer}>
                    <Searchbar />
                </View>
            </View>
            <View style={{ width: 'auto', height: '50%', marginLeft: 10, }}>
                <FoodCategoriesTab />
            </View>
        </>

    )
};

const styles = StyleSheet.create({
    headerTextContainer: {
        marginTop: 30,
        marginLeft: 15,
        width: '75%'
    },
    headerText: {
        color: '#000',
        fontSize: 32,
        fontWeight: '700'
    },
    searchContainer: {
        marginTop: 40,
        marginLeft: 25,
    }
});

export default DashboardHeader;