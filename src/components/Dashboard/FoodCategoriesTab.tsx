import { Tab, TabView, Text } from '@rneui/themed'
import React, { useState } from 'react'
import { ScrollView, View } from 'react-native';
import FoodTabItem from './FoodTabItem';

const FoodCategoriesTab = () => {

    const [idx, setIdx] = useState(0);

    return (
        <>
            <Tab
                value={idx}
                onChange={(e) => setIdx(e)}
                indicatorStyle={{
                    backgroundColor: '#FA4A0C',
                    height: 3,
                }}
                scrollable={true}
            >
                <Tab.Item
                    title="Foods"
                    titleStyle={{ fontSize: 16, color: '#FA4A0C' }}
                />
                <Tab.Item
                    title="Drinks"
                    titleStyle={{ fontSize: 16, color: '#FA4A0C' }}
                />
                <Tab.Item
                    title="Snacks"
                    titleStyle={{ fontSize: 16, color: '#FA4A0C' }}
                />
                <Tab.Item
                    title="Sauces"
                    titleStyle={{ fontSize: 16, color: '#FA4A0C' }}
                />
            </Tab>

            <TabView value={idx} onChange={setIdx} animationType="spring" disableSwipe={true}>
                <TabView.Item style={{ backgroundColor: '#F2F2F2', width: '100%', justifyContent: 'center' }}>
                    <FoodTabItem />
                </TabView.Item>
                <TabView.Item style={{ backgroundColor: '#F2F2F2', width: '100%' }}>
                    <FoodTabItem />
                </TabView.Item>
                <TabView.Item style={{ backgroundColor: '#F2F2F2', width: '100%' }}>
                    <FoodTabItem />
                </TabView.Item>
            </TabView>
        </>
    )
}

export default FoodCategoriesTab