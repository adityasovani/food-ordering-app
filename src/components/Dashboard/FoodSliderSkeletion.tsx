import { Skeleton } from '@rneui/themed'
import React from 'react'
import { View } from 'react-native'

const FoodSliderSkeletion = () => {
    return (
        <View style={{ marginTop: 30, marginRight: 25 }}>
            <Skeleton width={175} height={250} style={{ borderRadius: 25 }} />
        </View>
    )
}

export default FoodSliderSkeletion