import React, { useEffect, useState } from 'react'
import { View, Text, ScrollView, StyleSheet, TouchableOpacity } from 'react-native'
import supabaseHelper from '../../utils/supabase-helper';
import ProductPLPCard from './ProductCard/ProductCard';
import { useNavigation } from '@react-navigation/native';
import FoodSliderSkeletion from './FoodSliderSkeletion';

const FoodTabItem = () => {

    const [products, setproducts] = useState(null);

    async function getProducts() {
        const { data } = await supabaseHelper.from("Products").select("*");
        setproducts(data);
    }

    useEffect(() => {
        getProducts();
    }, []);

    return (
        <>
            <TouchableOpacity style={styles.viewMoreWrapper}>
                <Text style={styles.viewMoreText}>see more</Text>
            </TouchableOpacity>
            <ScrollView
                horizontal={true}
                style={{ width: 'auto' }}
            >
                {
                    (products)
                        ? products.map((product: any) => (
                            <ProductPLPCard key={product.id} product={product} />
                        ))
                        : ['.', '.'].map((_, idx) => <FoodSliderSkeletion key={idx} />)
                }
            </ScrollView>
        </>
    )
}

const styles = StyleSheet.create({
    viewMoreWrapper: {
        marginTop: 5,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-end',
        marginRight: 25,
    },
    viewMoreText: {
        color: '#FA4A0C',
        fontWeight: '500',
        textDecorationLine: 'underline'
    }
});

export default FoodTabItem;