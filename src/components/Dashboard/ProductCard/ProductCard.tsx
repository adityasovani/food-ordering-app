import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Card } from 'react-native-paper';
import Product from '../../../Model/Product';
import { useNavigation } from '@react-navigation/native';

type Props = {
    product: Product,
    handleLongPress?: Function
};

const ProductPLPCard: React.FC<Props> = ({ product }) => {
    const navigation = useNavigation<any>();

    const handleProductCardTouch = (productId: number) => {
        // alert(`Oye papaji. Card ID n di: ${productId}`)
        navigation.navigate('ProductDetails', { productId: productId });
    }

    return (
        <Card key={product.id} style={styles.productCard}
            onPress={() => handleProductCardTouch(product.id)}
        >
            <Card.Content>
                <Card.Cover source={{ uri: product.thumbnail_url }} />
                <View style={{ overflow: 'hidden' }}>
                    <Text style={styles.productName}>{product.name}</Text>
                </View>
                <View style={{}}>
                    <Text style={styles.price}>₹ {product.price}</Text>
                </View>
            </Card.Content>
        </Card>
    );
};

export default ProductPLPCard;

const styles = StyleSheet.create({
    productCard: {
        width: 'auto',
        minWidth: 200,
        height: 300,
        borderRadius: 30,
        marginRight: 10,
        marginTop: 20,
        backgroundColor: '#FFF',
    },
    productName: {
        alignSelf: 'center',
        color: '#000',
        marginTop: 10,
        fontSize: 20,
        fontWeight: '500',
    },
    price: {
        alignSelf: 'center',
        marginTop: 15,
        color: '#FA4A0C',
        fontWeight: '700',
        fontSize: 16
    }
});