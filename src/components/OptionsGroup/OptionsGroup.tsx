import { CheckBox, Icon } from '@rneui/themed';
import React, { useRef, useState } from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native'


type Props = {
    items: Array<OptionsGroupListItem> | Array<String>;
    selectedIndex: number;
    setIndex: Function
};

const OptionsGroup: React.FC<Props> = ({ items, selectedIndex, setIndex }) => {

    const isSimpleList = useRef(true);

    const Checkbox = ({ index }) => {
        return <CheckBox
            checked={selectedIndex === index}
            onPress={() => setIndex(index)}
            checkedIcon={<Icon
                name="radio-button-checked"
                type="material"
                color="#FA4A0C"
                size={25}
                iconStyle={{ marginRight: 10 }} />}
            uncheckedIcon={<Icon
                name="radio-button-unchecked"
                type="material"
                color="grey"
                size={25}
                iconStyle={{ marginRight: 10 }} />}
            checkedColor='#FA4A0C' />;
    }

    return (
        <View style={[styles.paymentMethodListCard]}>
            {
                items?.map((item: any, index: number) => {
                    (item?.icon && item?.color) ? isSimpleList.current = false : null;
                    return <View key={index}>
                        <TouchableOpacity
                            style={[styles.optionContainer, isSimpleList ? { height: 60 } : {}]}
                            onPress={() => setIndex(index)}>
                            <Checkbox index={index} />
                            {item?.icon && item?.color &&
                                <View style={[styles.iconWrapper, { backgroundColor: item.color }]}>
                                    <Image source={item.icon} />
                                </View>
                            }
                            <Text style={styles.optionName}>{item.name}</Text>
                        </TouchableOpacity>
                        {index !== items.length - 1 &&
                            <View style={{ borderBottomWidth: 0.5, width: '90%', alignSelf: 'center' }} />}
                    </View>;
                })
            }
        </View>
    )
};

const styles = StyleSheet.create({
    paymentMethodListCard: {
        width: '100%',
        height: 'auto',
        borderRadius: 20,
        backgroundColor: '#FFFFFF',
        shadowColor: 'rgba(0, 0, 0, 0.03)',
        left: 0,
        marginTop: 15,
        padding: 10,
    },
    optionContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 70,
    },
    iconWrapper: {
        height: 45,
        width: 45,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        left: -10
    },
    optionName: {
        color: '#000',
        fontSize: 16,
        fontWeight: '500',
    }
})

export default OptionsGroup;