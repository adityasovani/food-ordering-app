import { useNavigation } from "@react-navigation/native";
import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import { Appbar } from "react-native-paper";

type Props = {
    headerTitle?: string;
    showHeart?: boolean;
    heartPressAction?: Function;
};

const PageHeader: React.FC<Props> = ({ headerTitle, showHeart = false, heartPressAction }) => {

    const navigation = useNavigation<any>();
    const [heartIcon, setheartIcon] = useState('heart-outline');
    const [isHeartClicked, setIsHeartClicked] = useState(false);

    const handleHeartpress = () => {
        if (isHeartClicked) {
            setheartIcon('heart-outline');
            heartPressAction('unfavorite');
        } else {
            setheartIcon('heart');
            heartPressAction('favorite');
        }
        setIsHeartClicked(!isHeartClicked);
    }

    return (
        <Appbar.Header style={styles.appbarHeader}>
            <Appbar.Action icon={require('../../../assets/images/back-arrow.png')}
                accessibilityLabel="back" onPress={() => navigation.goBack()} />
            <Appbar.Content
                title={headerTitle}
                style={[styles.appBarTitleContainer, !showHeart ? { left: -20 } : {}]}
                titleStyle={styles.appbarTitle}
            />
            {showHeart && <Appbar.Action icon={heartIcon} onPress={handleHeartpress} />}
        </Appbar.Header>
    );
};

const styles = StyleSheet.create({
    appbarHeader: {
        marginTop: 5,
        backgroundColor: '#F5F5F8',
    },
    appBarTitleContainer: {
        alignItems: 'center',
    },
    appbarTitle: {
        fontSize: 16,
        color: '#000',
        fontWeight: '700',
    }
})

export default PageHeader;