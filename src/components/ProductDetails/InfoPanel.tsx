import React from 'react'
import { ScrollView } from 'react-native';
import { StyleSheet } from 'react-native';
import { Text } from 'react-native';
import { View } from 'react-native'

const InfoPanel = ({ title, content }) => {
    return (
        <View>
            <Text style={styles.title}>{title}</Text>
            <View style={styles.contentWrapper}>
                <ScrollView>
                    <Text style={styles.content}>{content}</Text>
                </ScrollView>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    title: {
        color: '#000',
        fontWeight: '600',
        fontSize: 16
    },
    contentWrapper: {
        marginTop: 5,
        height: 90
    },
    content: {
        fontSize: 13,
        color: 'grey'
    }
})

export default InfoPanel;