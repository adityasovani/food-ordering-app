import { Skeleton } from '@rneui/themed'
import React from 'react'
import { StyleSheet, View } from 'react-native'

const SkeletonProduct = () => {
    return (
        <View style={styles.imgSkeleton}>
            <Skeleton circle width={250} height={250} />
            <View style={{ marginTop: 30 }}>
                <Skeleton width={220} height={40} style={{ borderRadius: 30 }} />
            </View>
            <View style={{ marginTop: 20 }}>
                <Skeleton width={120} height={30} style={{ borderRadius: 30 }} />
            </View>

            <View style={{ marginTop: 60, alignSelf: 'flex-start' }}>
                <Skeleton width={120} height={30} style={{ borderRadius: 30 }} />
                <Skeleton width={220} height={100} style={{ borderRadius: 30, marginTop: 20 }} />
            </View>

            <View style={{ marginTop: 80 }}>
                <Skeleton width={220} height={50} style={{ borderRadius: 30 }} />
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    imgSkeleton: {
        alignItems: 'center'
    }
})

export default SkeletonProduct;