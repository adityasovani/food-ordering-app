import { useNavigation } from '@react-navigation/native';
import React from 'react'
import { View, StyleSheet } from 'react-native';
import { TextInput } from 'react-native-paper';

const Searchbar = () => {

    const navigation = useNavigation<any>();

    return (
        <View>
            <TextInput mode='outlined'
                left={<TextInput.Icon icon="magnify" style={{ paddingTop: 15 }} />}
                placeholder='Search'
                outlineStyle={{ borderRadius: 30, borderWidth: 0 }}
                contentStyle={{ paddingLeft: 10, height: 65 }}
                placeholderTextColor='grey'
                disabled={true}
                onPressIn={() => navigation.goBack()}
                style={styles.searchInput} />
        </View>
    )
}

const styles = StyleSheet.create({
    searchInput: {
        width: 300,
        height: 60,
        backgroundColor: '#EFEEEE',
    }
})

export default Searchbar;