import { View, Text } from 'react-native';
import Toast, { BaseToast, ErrorToast } from 'react-native-toast-message';

const toastConfig = {
  /*
    Overwrite 'success' type,
  */
  success: (props: any) => (
    <BaseToast
      {...props}
      style={{ borderLeftColor: 'lightgreen', height: 80, alignItems: 'center', justifyContent: 'center' }}
      contentContainerStyle={{ paddingHorizontal: 15, height: 30 }}
      text1Style={{
        fontSize: 15,
        fontWeight: '600'
      }}
      text2Style={{ fontSize: 13 }}
    />
  ),
  /*
    Overwrite 'error' type,
  */
  error: (props: any) => (
    <ErrorToast
      {...props}
      style={{ borderLeftColor: 'red', height: 80, alignItems: 'center', justifyContent: 'center' }}
      text1Style={{
        fontSize: 17
      }}
      text2Style={{
        fontSize: 15
      }}
    />
  ),
};

export default toastConfig;