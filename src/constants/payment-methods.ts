export const paymentMethodsAccount: Array<OptionsGroupListItem> = [
    { name: "Card", icon: require('../../assets/images/credit-card.png'), color: '#F47B0A' },
    { name: "Bank account", icon: require('../../assets/images/bank.png'), color: '#EB4796' },
    { name: "Paypal", icon: require('../../assets/images/paypal.png'), color: '#0038FF' },
];