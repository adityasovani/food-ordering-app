import Product from "../Model/Product";

export const ADD_TO_CART = "ADD_TO_CART";
export const REMOVE_FROM_CART = "REMOVE_FROM_CART";
export const ADD_TO_FAVORITES = "ADD_TO_FAVORITES";
export const REMOVE_FROM_FAVORITES = "REMOVE_FROM_FAVORITES";

let cartId = 0;

export const addToCart = (product: Product) => ({
    type: ADD_TO_CART,
    payload: {
        id: ++cartId,
        product
    }
});

export const removeFromCart = (productId: number) => ({
    type: REMOVE_FROM_CART,
    payload: {
        id: productId
    }
});

export const addToFavorites = (product: Product) => ({
    type: ADD_TO_FAVORITES,
    payload: {
        product
    }
});

export const removeFromFavorites = (id: number) => ({
    type: REMOVE_FROM_FAVORITES,
    payload: {
        id: id
    }
})