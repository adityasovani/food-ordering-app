import { ADD_TO_CART, ADD_TO_FAVORITES, REMOVE_FROM_CART, REMOVE_FROM_FAVORITES } from "./action";

const initialState = {
    cart: [],
    favourites: []
};

const cartReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case ADD_TO_CART: {
            const { id, product } = action.payload
            return {
                ...state,
                cart: [...state.cart, { id, product }]
            };
        };
        case REMOVE_FROM_CART: {
            const { id } = action.payload;
            return {
                ...state,
                cart: state.cart.filter(item => item.product.id !== id)
            };
        };
        case ADD_TO_FAVORITES: {
            const { product } = action.payload;
            return {
                ...state,
                favourites: [...state.favourites, product]
            };
        };
        case REMOVE_FROM_FAVORITES: {
            const { id } = action.payload;
            return{
                ...state,
                favourites: state.favourites.filter(item => item.id !== id)
            }
        };
        default:
            return state;
    }
};

export default cartReducer;