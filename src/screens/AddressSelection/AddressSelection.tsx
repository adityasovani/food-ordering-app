import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import PageHeader from '../../components/PageHeader/PageHeader';
import OptionsGroup from '../../components/OptionsGroup/OptionsGroup';
import { useSelector } from 'react-redux';
import Product from '../../Model/Product';
import { useNavigation } from '@react-navigation/native';

const AddressSelection = () => {

    const [selectedIndex, setIndex] = useState(0);
    const [products, setproducts] = useState<Array<Product>>([]);
    const cart = useSelector((state: any) => state.cart);
    const tempSum = products.map((product) => product.price).reduce((partialSum, a) => partialSum + a, 0)
    const navigation = useNavigation<any>();

    useEffect(() => {
        setproducts(cart.map((item: any) => item.product))
    }, [cart]);

    return (
        <View style={styles.addressContainer}>
            <PageHeader showHeart={false} headerTitle={'Checkout'} />
            <View style={{ width: '80%', alignSelf: 'center', maxHeight: '90%' }}>
                <Text style={styles.headerText}>Delivery</Text>
                <View style={styles.addressDetailsContainer}>
                    <View style={{ flexDirection: 'row', width: '65%', display: 'flex' }}>
                        <Text style={styles.addressDetailHeader}>Address Details</Text>
                        <TouchableOpacity>
                            <Text style={[styles.changeText]}>change</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.addressDetailsCard}>
                        <View style={[
                            styles.cardItem,
                            { marginTop: 5 }
                        ]}>
                            <Text style={{ fontSize: 16, fontWeight: '500', }}>Marvis Kparobo</Text>
                        </View>
                        <View style={[styles.cardItem]}>
                            <Text>Km 5 refinery road oppsite republic road, effurun, delta state</Text>
                        </View>
                        <View style={[styles.cardItem, { borderBottomWidth: 0 }]}>
                            <Text>+234 9011039271</Text>
                        </View>
                    </View>
                </View>
                <View style={[styles.addressDetailsContainer, { marginTop: '10%' }]}>
                    <View style={{ flexDirection: 'row', width: '65%', display: 'flex' }}>
                        <Text style={styles.addressDetailHeader}>Delivery Method</Text>
                    </View>
                    <OptionsGroup selectedIndex={selectedIndex}
                        items={[
                            { name: 'Door' }, { name: 'Pickup' }
                        ]}
                        setIndex={setIndex} />
                </View>
                <View style={{
                    flexDirection: 'row', marginTop: '25%', width: '100%'
                }}>
                    <Text style={{ fontSize: 17, fontWeight: '400' }}>Total</Text>
                    <Text style={{ marginLeft: 'auto', fontSize: 22, fontWeight: '600' }}>₹ {tempSum}</Text>
                </View>
                <TouchableOpacity style={styles.proceedButton} onPress={() => navigation.navigate('PaymentMethod')}>
                    <Text style={{ color: '#F6F6F9', fontSize: 16, fontWeight: '600' }}>Proceed</Text>
                </TouchableOpacity>
            </View>
        </View >
    )
};

const styles = StyleSheet.create({
    addressContainer: {
        flexDirection: 'column'
    },
    headerText: { fontSize: 30, fontWeight: '600', color: '#000', marginTop: 20 },
    addressDetailsContainer: {
        marginTop: '15%'
    },
    addressDetailHeader: {
        color: '#000',
        fontSize: 16,
        fontWeight: '600'
    },
    changeText: {
        alignSelf: 'flex-end',
        marginLeft: '70%',
        fontSize: 14,
        color: '#F47B0A',
        fontWeight: '400',
    },
    addressDetailsCard: {
        backgroundColor: '#FFF',
        marginTop: 20,
        flexDirection: 'column',
        padding: 10,
        borderRadius: 20
    },
    cardItem: { marginLeft: 10, borderBottomWidth: 0.5, marginTop: 5, paddingBottom: 5 },
    proceedButton: {
        backgroundColor: '#FA4A0C',
        borderRadius: 30,
        height: 60,
        marginTop: '20%',
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export default AddressSelection;