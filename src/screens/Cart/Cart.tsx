import React, { useEffect, useState } from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity, ScrollView } from 'react-native';
import PageHeader from '../../components/PageHeader/PageHeader'
import { useSelector } from 'react-redux'
import Product from '../../Model/Product';
import CartProductCard from '../../components/CartProductCard/CartProductCard';
import { Icon } from '@rneui/themed';
import { Button } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';

const Cart = () => {

    const cart = useSelector((state: any) => state.cart);
    const [products, setproducts] = useState<Array<Product>>([]);
    const navigation = useNavigation<any>();

    useEffect(() => {
        setproducts(cart.map((item: any) => item.product))
    }, [cart]);

    return (
        <View style={styles.cartPageContainer}>
            <PageHeader headerTitle='Cart' showHeart={false} />
            {
                products.length ?
                    <>
                        <View style={{ flexDirection: 'row', alignSelf: 'center', marginTop: 20 }}>
                            <Icon name='swipe'></Icon>
                            <Text style={{ fontSize: 14, fontWeight: '400' }}>swipe left on an item to delete</Text>
                        </View>
                        <ScrollView style={{ maxHeight: '70%', marginTop: 15 }}>
                            {
                                products.map((product: Product, idx: number) =>
                                    <View style={{}} key={idx} >
                                        <CartProductCard product={product} />
                                    </View>
                                )
                            }

                        </ScrollView>
                        <TouchableOpacity style={[styles.orderButton]}
                            onPress={() => navigation.navigate('AddressSelection')}>
                            <Text style={{ color: '#F6F6F9', fontSize: 16, fontWeight: '600' }}>Complete order</Text>
                        </TouchableOpacity>
                    </>
                    : <></>
            }
        </View >
    )
};

const styles = StyleSheet.create({
    cartPageContainer: {
        height: '100%',
        display: 'flex'
    },
    orderButton: {
        width: '80%',
        alignSelf: 'center',
        backgroundColor: '#FA4A0C',
        borderRadius: 30,
        height: 60,
        bottom: 25,
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center'
    }
});

export default Cart;