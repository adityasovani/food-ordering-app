import React, { useState } from 'react'
import { StyleSheet, View } from 'react-native'
import DashboardHeader from '../../components/Dashboard/DashboardHeader';
import { Tab, TabView, Text } from '@rneui/themed';
import { brown300 } from 'react-native-paper/lib/typescript/styles/themes/v2/colors';
import MyProfile from '../MyProfile/MyProfile';
import PageHeader from '../../components/PageHeader/PageHeader';
import OrderHistory from '../OrderHistory/OrderHistory';
import Favorites from '../Favorites/Favorites';

const Dashboard = () => {

    const [index, setIndex] = useState(0);

    return (
        <>
            <TabView value={index} onChange={setIndex} animationType="spring" disableSwipe={true}>
                <TabView.Item>
                    <DashboardHeader title='Delicious food for you' />
                </TabView.Item>
                <TabView.Item style={{ backgroundColor: '#F5F5F8', width: '100%' }}>
                    <Favorites />
                </TabView.Item>
                <TabView.Item style={{ backgroundColor: '#F5F5F8', width: '100%' }}>
                    <MyProfile />
                </TabView.Item>
                <TabView.Item style={{ backgroundColor: '#F5F5F8', width: '100%' }}>
                    <OrderHistory setIndex={setIndex} />
                </TabView.Item>
            </TabView>
            <Tab
                value={index}
                onChange={(e) => setIndex(e)}
                style={{ marginBottom: 5 }}
                indicatorStyle={{ display: 'none' }}
            >
                <Tab.Item
                    icon={{ name: 'home', type: 'ionicon', color: '#FA4A0C', }}
                />
                <Tab.Item
                    icon={{ name: 'heart', type: 'ionicon', color: '#FA4A0C', iconStyle: { borderColor: 'red' } }}
                />
                <Tab.Item
                    icon={{ name: 'person', type: 'ionicon', color: '#FA4A0C' }}
                />
                <Tab.Item
                    icon={{ name: 'timer', type: 'ionicon', color: '#FA4A0C' }}
                />
            </Tab>
        </>
    )
};

const styles = StyleSheet.create({
    dashboardContainer: {

    }
})

export default Dashboard