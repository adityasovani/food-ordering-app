import React from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';
import PageHeader from '../../components/PageHeader/PageHeader';
import { Icon, Text } from '@rneui/themed';
import ProductPLPCard from '../../components/Dashboard/ProductCard/ProductCard';
import { useDispatch, useSelector } from 'react-redux';
import Product from '../../Model/Product';
import GestureRecognizer from 'react-native-swipe-gestures';
import { removeFromFavorites } from '../../redux/action';

const Favorites = () => {

    const favoritesProducts = useSelector((state: any) => state.favourites);
    const dispatch = useDispatch();

    return (
        <>
            <PageHeader headerTitle='Favorites' />
            <View style={{ width: '95%', alignSelf: 'center', alignItems: 'center', maxWidth: 450, overflow: 'scroll', height: '85%' }}>
                <Text h2 style={{ alignSelf: 'center' }}>Favorites</Text>
                {favoritesProducts.length ?
                    (<View style={{ flexDirection: 'row', alignSelf: 'center', marginTop: 20 }}>
                        <Icon name='swipe'></Icon>
                        <Text style={{ fontSize: 14, fontWeight: '400' }}>swipe to delete</Text>
                    </View>) : null}
                {
                    favoritesProducts.length
                        ?
                        <ScrollView style={{ flexDirection: 'column', width: 300, display: 'flex' }}>
                            {
                                favoritesProducts.map((product: Product, index: number) => (
                                    <GestureRecognizer key={index}
                                        onSwipe={() => dispatch(removeFromFavorites(product.id))}>
                                        <View style={[]}>
                                            <ProductPLPCard product={product} />
                                        </View>
                                    </GestureRecognizer>

                                ))
                            }
                        </ScrollView>

                        : <View style={{ alignItems: 'center', justifyContent: 'center', display: 'flex', height: '70%' }}>
                            <Text style={{ fontSize: 16 }}>No items in Favorites</Text>
                        </View>
                }
            </View >
        </>
    )
};

const styles = StyleSheet.create({

})

export default Favorites;