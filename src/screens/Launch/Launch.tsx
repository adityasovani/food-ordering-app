import { useNavigation } from '@react-navigation/native';
import React from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity, } from 'react-native';
import { Button } from 'react-native-paper';
import { useFonts } from 'expo-font';

const Launch: React.FC = () => {

    const navigation = useNavigation<any>();

    useFonts({
        "SF Pro Rounded": require('../../../assets/fonts/SF-Pro-Rounded-Regular.ttf')
    });

    return (
        <View style={[styles.mainView]}>
            <View style={[styles.titleHeader]}>
                <Text style={[styles.titleText]}>Food for Everyone who says</Text>
            </View>
            <View style={[styles.titleContent]}>
                <Image
                    style={[styles.image1]}
                    source={require('../../../assets/images/ToyFaces_Tansparent_BG_49.png')}
                />
                <Image
                    style={[styles.image2]}
                    source={require('../../../assets/images/ToyFaces_Tansparent_BG_29.png')}
                />
            </View>
            <View style={[styles.launchFooter]}>
                <Button style={[styles.footerButton]} onPress={()=>navigation.navigate('Login')}>
                    <Text style={[styles.buttonText]}>Get started</Text>
                </Button>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    mainView: {
        backgroundColor: '#FF4B3A',
        height: '100%',
    },
    titleHeader: {
        marginTop: 100,
    },
    titleText: {
        textAlign: 'center',
        color: "#FFF",
        // fontFamily: "SF-Pro-Rounded-Regular",
        fontSize: 50,
        "fontStyle": "normal",
        "fontWeight": "800",
        lineHeight: 50,
    },
    titleContent: {
        marginTop: 60,
    },
    image1: {
        width: 290,
        height: 400,
        justifyContent: 'center', // Vertically center content
        resizeMode: 'cover',
        position: 'absolute',
    },
    image2: {
        marginLeft: 190,
        marginTop: 70
    },
    launchFooter: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 25
    },
    footerButton: {
        borderRadius: 40,
        backgroundColor: '#FFF',
        width: 290,
        height: 65,
        alignItems:'center',
        justifyContent:'center',

    },
    buttonText: {
        color: '#FF460A',
        fontSize: 17,
        fontWeight: '600'
    }
})

export default Launch;