import React from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { createMaterialBottomTabNavigator } from 'react-native-paper/lib/typescript/react-navigation';
import TabNav from './TabNav';

const Login: React.FC = () => {
	return (
		<>
			<View style={[styles.mainViewHeader]}>
				<Image source={require('../../../assets/images/chef-icon.png')} />
			</View>
			<TabNav />
		</>
	)
}

const styles = StyleSheet.create({
	mainViewHeader: {
		display: 'flex',
		height: '35%',
		backgroundColor: '#FFF',
		justifyContent: 'center',
		alignItems: 'center'
	},
	tabsContainer: {
	}
});

export default Login;