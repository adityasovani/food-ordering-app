import { useNavigation } from '@react-navigation/native';
import { Button } from '@rneui/themed';
import React, { useEffect, useState } from 'react'
import { View, StyleSheet, TouchableOpacity } from 'react-native'
import { Text, TextInput } from 'react-native-paper'

const LoginForm = () => {

    const [emailAddress, setemailAddress] = useState<string>("");
    const [password, setPassword] = useState<string>("");
    const [isFormValid, setisFormValid] = useState(false);
    const navigation = useNavigation<any>();

    useEffect(() => {
        const emailRegex = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
        const passwordRegex = /[^\s\n]+/;

        setisFormValid(emailRegex.test(emailAddress) && passwordRegex.test(password));

    }, [emailAddress, password]);


    return (
        <View style={[styles.loginContainer]}>
            <TextInput label='Email address'
                value={emailAddress}
                textColor='#000'
                underlineColor='#000'
                activeUnderlineColor='#000'
                onChangeText={text => setemailAddress(text)}
                style={[styles.emailInput]} />
            <TextInput
                value={password}
                onChangeText={text => setPassword(text)}
                secureTextEntry={true}
                label='Password'
                textColor='#000'
                underlineColor='#000'
                activeUnderlineColor='#000'
                style={[{ ...styles.emailInput }, styles.passwordField]}
            />
            <TouchableOpacity style={styles.forgotPassContainer}>
                <Text style={styles.forgotText}>Forgot Password ?</Text>
            </TouchableOpacity>
            <View style={{ marginTop: 130, alignItems: 'center' }}>
                <Button
                    buttonStyle={[
                        styles.loginButton,
                        { justifyContent: 'center' }
                    ]}
                    disabledStyle={{ ...styles.loginButton, backgroundColor: 'lightgrey' }}
                    // disabled={!isFormValid}
                    onPress={() => navigation.navigate('Dashboard')}
                >
                    <Text style={[styles.btnText]}>Login</Text>
                </Button>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    loginContainer: {
        width: '100%',
        marginLeft: '10%',
        marginTop: 60,
        display: 'flex'
    },
    emailInput: {
        display: 'flex',
        alignSelf: 'center',
        width: 300,
        backgroundColor: '#F2F2F2'
    },
    passwordField: {
        marginTop: '10%',
    },
    forgotPassContainer: {
        marginTop: 40,
        marginLeft: 30
    },
    forgotText: {
        color: '#FA4A0C',
        fontWeight: '600',
        fontSize: 16
    },
    loginButton: {
        width: 314,
        height: 70,
        borderRadius: 35,
        backgroundColor: '#FA4A0C'
    },
    btnText: {
        color: '#F6F6F9',
        fontWeight: '700',
        fontSize: 16,
    }
})

export default LoginForm;