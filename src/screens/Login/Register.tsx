import React from 'react'
import { StyleSheet, View } from 'react-native';
import { Text } from 'react-native-paper'

const Register = () => {
    return (
        <View style={[]}>
            <Text style={styles.registrationContainer}>Registration is not in Figma. Its not my fault</Text>
        </View>
    )
};

const styles = StyleSheet.create({
    registrationContainer: {
        marginTop:50,
        marginLeft:50
    }
})

export default Register;