import { TabView } from '@rneui/base';
import { Tab, Text } from '@rneui/themed'
import React, { useState } from 'react'
import LoginForm from './LoginForm';
import Register from './Register';
import { StyleSheet } from 'react-native';

const TabNav = () => {

    const [index, setIndex] = useState<number>(0);
    return (
        <>
            <Tab
                value={index}
                onChange={(e) => setIndex(e)}
                indicatorStyle={{
                    backgroundColor: '#FA4A0C',
                    height: 3,
                    width: '20%',
                    marginLeft: '10%'
                }}
                style={[{
                    backgroundColor: '#FFF',
                    borderBottomRightRadius: 25,
                    borderBottomLeftRadius: 25
                }]}
            >
                <Tab.Item
                    title="Login"
                    titleStyle={styles.titleStyle}
                />
                <Tab.Item
                    title="Sign-up"
                    titleStyle={styles.titleStyle}
                />
            </Tab>

            <TabView value={index} onChange={setIndex} animationType="spring">
                <TabView.Item style={[]}>
                    <LoginForm />
                </TabView.Item>
                <TabView.Item style={[]}>
                    <Register />
                </TabView.Item>
            </TabView>
        </>
    );
}

const styles = StyleSheet.create({
    titleStyle: { fontSize: 16, color: '#000' }
})

export default TabNav;