import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import PageHeader from '../../components/PageHeader/PageHeader';
import { Button, Card } from 'react-native-paper';
import supabaseHelper from '../../utils/supabase-helper';
import { Image } from 'react-native';
import { Icon } from '@rneui/themed';
import OptionsGroup from '../../components/OptionsGroup/OptionsGroup';
import { paymentMethodsAccount } from '../../constants/payment-methods';

const MyProfile = () => {

    const [profile, setprofile] = useState<Profile>(null);
    const [selectedIndex, setIndex] = useState(0);

    useEffect(() => {
        getProfile();
    }, []);


    const getProfile = async () => {
        const { data: Profile, error } = await supabaseHelper
            .from('Profile')
            .select('*');
        setprofile(Profile[0]);
        error && alert(error);
    };

    return (
        <View>
            <PageHeader headerTitle='My Profile' showHeart={false} />
            <View style={styles.infoContainer}>
                <Text style={styles.intoText}>Information</Text>
                <Card style={styles.infoCard}>
                    <View style={{ flexDirection: 'row', marginTop: 15, marginLeft: 10, }}>
                        {
                            profile
                                ? <>
                                    <View style={styles.dp}>
                                        <Image source={{ uri: profile.profile_picture_url }}
                                            style={{ height: 60, width: 60 }} />
                                    </View>
                                    <View style={{ maxWidth: '65%', marginLeft: '5%' }}>
                                        <Text style={{ fontSize: 18, fontWeight: '600' }}>{profile.name}</Text>
                                        <Text style={{ marginTop: 5, fontSize: 13, fontWeight: '400' }}>{profile.email}</Text>
                                        <Text style={{ marginTop: 5, fontSize: 12, fontWeight: '400' }}>{profile.address}</Text>
                                    </View>
                                    <View>
                                        <Text>
                                            <Icon name='edit' />
                                        </Text>
                                    </View>
                                </>
                                : <>
                                    <Text>Loading...</Text>
                                </>
                        }
                    </View>
                </Card>
                <View style={styles.paymentMethodContainer}>
                    <Text style={styles.intoText}>Payment  Method</Text>
                    <OptionsGroup items={paymentMethodsAccount}
                        selectedIndex={selectedIndex}
                        setIndex={setIndex} />
                </View>
                <Button style={styles.updateProfileButton}>
                    <Text style={{ color: '#F6F6F9', fontWeight: '600', fontSize: 16 }}>Update</Text>
                </Button>
            </View>
        </View>
    )
};

const styles = StyleSheet.create({
    infoContainer: {
        marginTop: '7%',
        width: '90%',
        marginLeft: '7%',
    },
    intoText: {
        color: '#000',
        fontSize: 16,
        fontWeight: '600',
    },
    infoCard: {
        marginTop: 15,
        left: -10,
        height: 120,
        width: '100%',
        backgroundColor: '#FFFFFF'
    },
    dp: {
        display: 'flex',
        width: 70,
        alignItems: 'center'
    },
    paymentMethodContainer: {
        marginTop: '15%'
    },
    updateProfileButton: {
        width: '90%',
        marginTop: '20%',
        backgroundColor: '#FA4A0C',
        borderRadius: 30,
        height: 60,
        justifyContent: 'center',
        alignSelf: 'center'
    }
});

export default MyProfile;