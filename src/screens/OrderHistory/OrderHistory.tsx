import React from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'
import PageHeader from '../../components/PageHeader/PageHeader';
import { Button } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';

const OrderHistory = ({ setIndex }) => {

    const navigation = useNavigation<any>();

    return (
        <>
            <PageHeader showHeart={false} headerTitle='Orders' />
            <View style={styles.historyContainer}>
                <View style={{ width: 220, }}>
                    <Image source={require('../../../assets/images/cart-big.png')} />
                    <Text style={{ fontSize: 26, fontWeight: '600' }}>No Orders Yet</Text>
                    <Text style={{ marginTop: 5, color: 'grey' }}>Hit the orange button down below to Create an order</Text>
                </View>
                <Button style={styles.orderingButton} onPress={() => setIndex(0)}>
                    <Text style={{ color: '#F6F6F9', fontWeight: '600', fontSize: 16 }}>Start Ordering</Text>
                </Button>
            </View>
        </>
    )
};

const styles = StyleSheet.create({
    historyContainer: {
        marginTop: '50%',
        display: 'flex',
        height: '80%',
        alignItems: 'center',
        width: 'auto',
        right: -25,

    },
    orderingButton: {
        width: 300,
        marginTop: '50%',
        backgroundColor: '#FA4A0C',
        left: -25,
        borderRadius: 30,
        height: 60,
        justifyContent: 'center'
    }
})

export default OrderHistory