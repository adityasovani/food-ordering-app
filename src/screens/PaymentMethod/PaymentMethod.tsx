import React, { useState } from 'react'
import PageHeader from '../../components/PageHeader/PageHeader'
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native'
import OptionsGroup from '../../components/OptionsGroup/OptionsGroup';
import { paymentMethodsAccount } from '../../constants/payment-methods';

const PaymentMethod = () => {

    const [selectedIndex, setIndex] = useState(0);

    return (
        <>
            <PageHeader showHeart={false} headerTitle='Payment' />
            <View style={styles.paymentcontainer}>
                <Text style={styles.addressDetailHeader}>Payment Methods</Text>
                <OptionsGroup items={paymentMethodsAccount}
                    selectedIndex={selectedIndex}
                    setIndex={setIndex} />
                <TouchableOpacity style={styles.placeOrderButton} onPress={()=>alert("Kashala baher khatay. Gap ghari kha.")}>
                    <Text style={{ color: '#F6F6F9', fontSize: 17, fontWeight: '600' }}>Place order</Text>
                </TouchableOpacity>
            </View>
        </>
    )
};

const styles = StyleSheet.create({
    paymentcontainer: {
        width: '80%',
        height: '100%',
        alignSelf: 'center',
        marginTop: '30%'
    },
    addressDetailHeader: {
        color: '#000',
        fontSize: 16,
        fontWeight: '600'
    },
    placeOrderButton: {
        marginTop: '55%',
        backgroundColor: '#FA4A0C',
        borderRadius: 30,
        height: 60,
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export default PaymentMethod