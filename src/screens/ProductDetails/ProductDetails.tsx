import React, { useEffect, useState } from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import PageHeader from '../../components/PageHeader/PageHeader';
import supabaseHelper from '../../utils/supabase-helper';
import Product from '../../Model/Product';
import InfoPanel from '../../components/ProductDetails/InfoPanel';
import { Avatar, Button } from 'react-native-paper';
import SkeletonProduct from '../../components/ProductDetails/SkeletonProduct';
import Toast from 'react-native-toast-message';
import { useDispatch } from 'react-redux';
import { addToCart, addToFavorites, removeFromFavorites } from '../../redux/action';

const ProductDetails = ({ route, navigation }) => {

    const { productId } = route.params
    const [product, setproducts] = useState<Product>(null);
    const dispatch = useDispatch();

    async function getProductDetail() {
        const { data: Products, error } = await supabaseHelper
            .from('Products')
            .select("*")
            .eq('id', productId);
        setproducts(Products[0]);
        error && alert('Error occured! Please try again.');
    }

    useEffect(() => {
        getProductDetail();
    }, []);

    const addProductToCart = () => {
        let toastConfig = {
            type: 'success',
            text1: 'Done',
            text2: 'Product added to cart!',
            time: 2000
        }
        try {
            dispatch(addToCart(product));
        } catch (err) {
            toastConfig = {
                type: 'error',
                text1: 'Error',
                text2: 'Error adding product to your cart. Please try again',
                time: 3000
            }
        }
        Toast.show(toastConfig);
    };

    const heartPressAction = (action: 'favorite' | 'unfavorite') => {
        if (action === 'favorite') {
            dispatch(addToFavorites(product));
        } else if (action == 'unfavorite') {
            dispatch(removeFromFavorites(product.id));
        }
    };

    return (
        <View style={styles.PDPContainer}>
            <PageHeader showHeart={true} heartPressAction={heartPressAction} />
            <View style={{ width: '80%', alignSelf: 'center' }}>
                {
                    product ?
                        <View style={styles.productWrapper}>
                            <Image source={{ uri: product.thumbnail_url }} style={styles.imageContainer} />
                            <Text style={styles.productName}>{product.name}</Text>
                            <Text style={styles.price}>₹ {product.price}</Text>

                            <View style={styles.descriptionContainer}>
                                <InfoPanel title='Description' content={product.description} />
                            </View>

                            <View style={{ marginTop: 10 }}>
                                <InfoPanel title='Delivery info' content='Delivered between monday aug and thursday 20 from 8pm to 9:30 pm' />
                            </View>

                            <View style={{ marginTop: 0 }}>
                                <Button style={styles.addToCart} onPress={() => addProductToCart()}>
                                    <Text style={styles.addButtonText}>Add to cart</Text>
                                </Button>
                            </View>

                        </View>
                        : <SkeletonProduct />
                }
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    PDPContainer: {
        backgroundColor: '#FFF',
        height: '100%',
        width: '100%',
    },
    productWrapper: { marginTop: 20 },
    imageContainer: { height: 269 },
    productName: {
        marginTop: 40,
        textAlign: 'center',
        color: '#000',
        fontWeight: '600',
        fontSize: 28
    },
    price: {
        color: '#FA4A0C',
        marginTop: 15,
        textAlign: 'center',
        fontWeight: '700',
        fontSize: 19
    },
    descriptionContainer: {
        marginTop: 20
    },
    addToCart: {
        backgroundColor: '#FA4A0C',
        height: 65,
        borderRadius: 30,
        justifyContent: 'center'
    },
    addButtonText: {
        color: '#F6F6F9',
        fontSize: 16,
        fontWeight: '600'
    }
})

export default ProductDetails;